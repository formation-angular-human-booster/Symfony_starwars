<?php

namespace App\Entity;

use App\Repository\PlaneteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppConstraint;

/**
 * @ORM\Entity(repositoryClass=PlaneteRepository::class)
 */
class Planete
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="Impossible : le nom de la planète ne doit pas être null")
     * @AppConstraint\OnlyLetter(message="Pas de chiffres dans le nom de la planète")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull(message="Impossible : le nom du terrain doit être saisire")
     */
    private $terrain;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull(message="Impossible : le nom du terrain doit être saisire")
     * @Assert\GreaterThan(
     *     value = 300,
     *     message= "Impossible il n'y a pas de planète à moins de 300 km de la terre donc {{ value }} n'est pas valide"
     * )
     */
    private $nbKmTerre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allegiance;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="date", name="date_decouverte")
     */
    private $deteDecouverte;

    /**
     * @ORM\OneToMany(targetEntity=Resident::class, mappedBy="planete", fetch="EAGER", cascade={"remove"})
     */
    private $rensidents;

    public function __construct()
    {
        $this->rensidents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTerrain(): ?string
    {
        return $this->terrain;
    }

    public function setTerrain(?string $terrain): self
    {
        $this->terrain = $terrain;

        return $this;
    }

    public function getNbKmTerre(): ?int
    {
        return $this->nbKmTerre;
    }

    public function setNbKmTerre(int $nbKmTerre): self
    {
        $this->nbKmTerre = $nbKmTerre;

        return $this;
    }

    public function getAllegiance(): ?string
    {
        return $this->allegiance;
    }

    public function setAllegiance(?string $allegiance): self
    {
        $this->allegiance = $allegiance;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDeteDecouverte(): ?\DateTimeInterface
    {
        return $this->deteDecouverte;
    }

    public function setDeteDecouverte(\DateTimeInterface $deteDecouverte): self
    {
        $this->deteDecouverte = $deteDecouverte;

        return $this;
    }

    /**
     * @return Collection|Resident[]
     */
    public function getRensidents(): Collection
    {
        return $this->rensidents;
    }

    public function addRensident(Resident $rensident): self
    {
        if (!$this->rensidents->contains($rensident)) {
            $this->rensidents[] = $rensident;
            $rensident->setPlanete($this);
        }

        return $this;
    }

    public function removeRensident(Resident $rensident): self
    {
        if ($this->rensidents->contains($rensident)) {
            $this->rensidents->removeElement($rensident);
            // set the owning side to null (unless already changed)
            if ($rensident->getPlanete() === $this) {
                $rensident->setPlanete(null);
            }
        }

        return $this;
    }
}
