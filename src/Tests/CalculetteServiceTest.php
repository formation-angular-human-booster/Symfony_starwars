<?php

namespace App\Tests\Util;

use App\Services\CalculetteService;
use PHPUnit\Framework\TestCase;

class CalculetteServiceTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new CalculetteService();
        $result = $calculator->additionner(30, 12);
        $this->assertEquals(42, $result);
    }
}

?>