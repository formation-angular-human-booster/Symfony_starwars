<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testCantConnect()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginForm(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $this->assertSame(1, $crawler->filter('button:contains("Sign in")')->count());
        $this->assertSame(1,  $crawler->filter('#inputEmail')->count());
        $this->assertSame(1,  $crawler->filter('#inputPassword')->count());

        $form = $crawler->selectButton('Sign in')->form();
        $form['email'] = 'aureliendelorme1@gmail.com';
        $form['password'] = 'aurelien';
        $crawler = $client->submit($form);

        $this->assertSame(1, $crawler->filter('body:contains("done")')->count());
        $this->assertSame(1, $crawler->filter('body:contains("done")')->count());
        $this->assertSame(1, $crawler->filter('body:contains("aureliendelorme1@gmail.com")')->count());
    }

}